gmt
===

GPRO Manager's Toolbox

Segue tutorial de como calcular os campos da planilha trackadditionaldata.xls(csv),
que é a planilha utilizada para calcular os setups das córrida.

COMPOUND COEF
--------------------------------------------------------------------
TCD = k_track * (50 - temperatura) + k_supplier

   Onde K são constantes, e k_supplier para pipis é igual a 0. Como
sabemos os valores de tudo, dá para calcular o k_track:

0,805 = k_track * (50 - 7) + 0
k_track = 0,01872093

   Só acrescentar isso na planilha tmb, na coluna "Compound Coef".


FUEL COF
--------------------------------------------------------------------
fuel_cof = Percentual de uso de combustível na chuva... tem que pegar uma corrida que tenha um stint completo na chuva e ver quantos litros gastou comparado com o seco. Se gastou 80%, então fuel coef é 0.8.


SETUP WINGS	SETUP ENGINE	SETUP BRAKES	SETUP GEAR	SETUP SUSP (ESSES SÃO POSSÍVEIS APENAS VIA GERAÇÃO DE CÓDIGO JAVA)
--------------------------------------------------------------------
Os campos:
Setup Wings	/ Setup Engine / Setup Brakes /	Setup Gear / Setup Susp:

O cara que criou essa fórmula parou de jogar a muito tempo e não nos
passou o modelo para fazer a regressão. Então o único jeito é força
bruta mesmo.

Um programinha besta para fazer isso:

http://pastebin.com/6WSZX42j
Ou
Execute no projeto: NewTrackCoefCalculator

Basicamente vc pega alguém que tenha achado o setup ideal e preenche nas linhas 4 a 24 do
programa acima os valores dos atributos do piloto e carro do cara, bem
como temperatura. Roda o programa e ele vai tentar todos os
coeficientes até achar aqueles que dão o resultado correto. Aí é
adicionar na planilha e pronto.

--------------------------------------------------------------------
WING SPLIT / WING NORMAL
NÃO APRENDI O QUE PREENCHER
--------------------------------------------------------------------


--------------------------------------------------------------------

Pega a telemetria de alguém que fez a corrida toda sem quebrar o carro. Aí
baseado naquela telemetria preenche na planilha:

F_con = concentração do piloto
F_agr = agressividade
F_exp = experiência
F_TeI = conhecimento técnico (technical insight)
F_eng = nível do motor (engine)
F_ele = nível dos eletrônicos
F_hum = média de umidade da corrida
F_fue = combustível total gasto


--------------------------------------------------------------------
Mais informações:
Para buscar o TCD precisa colocar a temperatura do practice/Q1, exemplo se for 7:
0,805 = k_track * (50 - 7) + 0
k_track = 0,01872093

--------------------------------------------------------------------
Finalizar e executar o projeto:

Exportar planilha para csv.
Depois de exportar o .xls para .csv, vc tem que executar o
CarWearSpreadsheetLoader para gerar o arquivo refdata.bin atualizado

como está adicionando/mudando dados na planilha de coeficientes,
faltou alterar o classe DatabaseUpdateManager para a versão nova do
schema (30 ao invés de 29):

private static final int LAST_REFDATA_UPDATE = 30

mvn clean install assembly:assembly

mvn antrun:run

mvn assembly:assembly -Dmaven.test.skip=true




