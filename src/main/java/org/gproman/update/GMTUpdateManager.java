package org.gproman.update;

import java.io.ByteArrayInputStream;
import java.util.Properties;

import org.gproman.Version;
import org.gproman.scrapper.GPROBrUtil;
import org.gproman.scrapper.WebConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

public class GMTUpdateManager {

    private static final Logger logger = LoggerFactory.getLogger(GMTUpdateManager.class);

    private String              updateInfoURL;
    private UpdateInfo          info;

    private GPROBrUtil browser;

    public GMTUpdateManager(GPROBrUtil browser, String updateInfoURL) {
        this.browser = browser;
        this.updateInfoURL = updateInfoURL;
    }

    public GMTUpdateManager checkLatestVersion() {
        try {
            logger.info("Checking latest version...");
            HtmlPage page = browser.getPage(updateInfoURL);
            
            if( page != null ) {
                HtmlTable table = page.getFirstByXPath("//table[@class='post_content_table']");
                if( table != null ) {
                    HtmlTableRow row = table.getRow(0);
                    HtmlTableCell cell = row.getCell(1);

                    HtmlTableRow row2 = table.getRow(1);
                    HtmlTableCell cell2 = row2.getCell(1);

                    String versao = cell.getTextContent();
                    String url = cell2.getTextContent();

                    info = new UpdateInfo(new Version(versao),url,"");
                    logger.info("Latest available version = " + info.getLatestVersion());
                }
            }
        } catch (Exception e) {
            logger.error("Unable to retrieve latest version information", e);
            info = null;
        }
        return this;
    }

    public UpdateInfo getUpdateInfo() {
        return info;
    }

}
