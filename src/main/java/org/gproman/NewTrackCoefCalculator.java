/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gproman;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import org.gproman.calc.SetupCalculator;
import org.gproman.model.car.Car;
import org.gproman.model.car.CarPart;
import org.gproman.model.driver.Driver;
import org.gproman.model.driver.DriverAttributes;
import org.gproman.model.race.Weather;
import org.gproman.model.season.TyreSupplier;
import org.gproman.model.track.Track;

/**
 *
 * @author renan
 */
public class NewTrackCoefCalculator {

    public static void main(String[] args) {
        BigDecimal tcd = new BigDecimal("0.363");
        TyreSupplier supplier = TyreSupplier.PIPIRELLI;
        int temp = 24;
        Weather weather = Weather.PARTIALLY_CLOUDY;

        Car car = new Car();
        car.setChassis(new CarPart("Cha", 6, 52));
        car.setEngine(new CarPart("Eng", 6, 8));
        car.setFrontWing(new CarPart("Fwg", 6, 30));
        car.setRearWing(new CarPart("Rwg", 7, 20));
        car.setUnderbody(new CarPart("Und", 7, 12));
        car.setSidepods(new CarPart("Sid", 8, 16));
        car.setCooling(new CarPart("Coo", 7, 44));
        car.setGearbox(new CarPart("Gea", 5, 41));
        car.setBrakes(new CarPart("Bra", 7, 20));
        car.setSuspension(new CarPart("Sus", 16, 30));
        car.setElectronics(new CarPart("Ele", 52,25));

        DriverAttributes driver = new DriverAttributes(152, 153, 232, 0, 114, 106, 158, 78, 240, 44, 49, 26);

        int[] idealSetup = new int[] { 623, 518, 425, 634, 646 };
        int[] setupCoefs = new int[] { 500, 500, 500, 500, 500 };

        Driver d = new Driver();
        d.setAttributes(driver);
        
        Calc[] calc = new Calc[] {
                new Calc() {
                    @Override public int calculate(Track track, int temp, Weather weather, Car car,  Driver driver) {
                        return SetupCalculator.calculateWings( track, temp, weather, car, driver );
                    }
                    @Override public void setCoef(Track track, int coef) {
                        track.setSetupWings( coef );
                    }
                },
                new Calc() {
                    @Override public int calculate(Track track, int temp, Weather weather, Car car, Driver driver) {
                        return SetupCalculator.calculateEngine(track, temp, weather, car, driver);
                    }
                    @Override public void setCoef(Track track, int coef) {
                        track.setSetupEngine( coef );
                    }
                },
                new Calc() {
                    @Override public int calculate(Track track, int temp, Weather weather, Car car, Driver driver) {
                        return SetupCalculator.calculateBrakes(track, temp, weather, car, driver);
                    }
                    @Override public void setCoef(Track track, int coef) {
                        track.setSetupBrakes( coef );
                    }
                },
                new Calc() {
                    @Override public int calculate(Track track, int temp, Weather weather, Car car,Driver driver) {
                        return SetupCalculator.calculateGear(track, temp, weather, car, driver);
                    }
                    @Override public void setCoef(Track track, int coef) {
                        track.setSetupGear( coef );
                    }
                },
                new Calc() {
                    @Override public int calculate(Track track, int temp, Weather weather, Car car,Driver driver) {
                        return SetupCalculator.calculateSuspension(track, temp, weather, car, driver);
                    }
                    @Override public void setCoef(Track track, int coef) {
                        track.setSetupSuspension( coef );
                    }
                },
        };

        Track track = new Track();
        for( int i = 0; i < idealSetup.length; i++ ) {
            track.setSetupWings( setupCoefs[i] );
            int result = Integer.MIN_VALUE;
            int count = 0;
            while( result != idealSetup[i] && count++ < 1000) {
                setupCoefs[i] += (result < idealSetup[i] ? +1 : -1);
                calc[i].setCoef(track, setupCoefs[i] );
                result = calc[i].calculate(track, temp, weather, car, d);
                //System.out.printf("Calc[%d] ideal=%d coef=%d result=%d\n", i, idealSetup[i], setupCoefs[i], result);
            }
        }

        System.out.format("TCD coef = %s\n", calcTCDCoef(tcd, supplier, temp).toString());
        System.out.printf("Setup coefs = %s\n", Arrays.toString(setupCoefs));
    }

    public static BigDecimal calcTCDCoef(BigDecimal tcd, TyreSupplier supplier, int temp ) {
        return tcd.subtract( supplier.getCompoundDiff() ).divide( new BigDecimal(50 - temp), RoundingMode.HALF_UP);
    }

    public static interface Calc {
        public int calculate( Track track, int temp, Weather weather, Car car, Driver driver );
        public void setCoef( Track track, int coef );
    }


}